package com.epam.jamp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jamp.aop.SaveToDB;
import com.epam.jamp.dao.PeopleDao;
import com.epam.jamp.models.People;

@Component
public class PeopleService {

	@Autowired
	private PeopleDao peopleDao;

	@SaveToDB
	@Transactional
	public void addPeople(People people) {
		peopleDao.addPeople(people);
	}

	public People getPersonById(String email) {
		return peopleDao.getPersonById(email);
	}

	public List<People> getAll() {
		return peopleDao.getAll();
	}
}
