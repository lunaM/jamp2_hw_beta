package com.epam.jamp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.epam.jamp.aop.SaveToDB;
import com.epam.jamp.models.MentorshipProgram;

@Repository
public class MentorshipProgramDao {

	@PersistenceContext
	EntityManager entityManager;

	public List<MentorshipProgram> getAllMentorshipPrograms() {
		String jpql = "SELECT DISTINCT mp FROM MentorshipProgram mp";
		TypedQuery<MentorshipProgram> q = entityManager.createQuery(jpql, MentorshipProgram.class);
		return q.getResultList();
	}

	@Transactional
	@SaveToDB
	public void addMentorshipProgram(MentorshipProgram mp) {
		entityManager.merge(mp);
	}

	public MentorshipProgram getMentorshipProgram(Long mpId) {
		String jpql = "SELECT DISTINCT mp FROM MentorshipProgram mp WHERE mp.id = :mpId";
		TypedQuery<MentorshipProgram> q = entityManager.createQuery(jpql, MentorshipProgram.class);
		q.setParameter("mpId", mpId);
		MentorshipProgram mp = null;
		try {
			mp = q.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return mp;
	}
}
