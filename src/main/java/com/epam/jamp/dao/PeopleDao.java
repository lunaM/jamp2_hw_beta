package com.epam.jamp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.epam.jamp.models.People;

@Repository
public class PeopleDao {

	@PersistenceContext
	EntityManager entityManager;

	public void addPeople(People people) {
		entityManager.merge(people);
	}

	public People getPersonById(String email) {
		String jpql = "SELECT DISTINCT p FROM People p WHERE p.email LIKE :email";
		TypedQuery<People> q = entityManager.createQuery(jpql, People.class);
		q.setParameter("email", email);
		People mp = null;
		try {
			mp = q.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
		return mp;
	}

	public List<People> getAll() {
		String jpql = "SELECT DISTINCT p FROM People p";
		TypedQuery<People> q = entityManager.createQuery(jpql, People.class);
		return q.getResultList();
	}
}
