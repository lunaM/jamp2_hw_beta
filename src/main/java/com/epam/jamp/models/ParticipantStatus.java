package com.epam.jamp.models;

public enum ParticipantStatus {
	PROPOSED, APPROVED_RM, CONFIRMED_CDP, ON_HOLD
}
