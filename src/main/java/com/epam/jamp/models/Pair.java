package com.epam.jamp.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Pair {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "group_id")
	private Long groupId;
	private String mentor;
	private String mentee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Pair() {
	}

	public Pair(String mentee, String mentor) {
		this.mentor = mentor;
		this.mentee = mentee;
	}

	public String getMentor() {
		return mentor;
	}

	public void setMentor(String mentor) {
		this.mentor = mentor;
	}

	public String getMentee() {
		return mentee;
	}

	public void setMentee(String mentee) {
		this.mentee = mentee;
	}
}
