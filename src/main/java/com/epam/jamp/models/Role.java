package com.epam.jamp.models;

public enum Role {
	MENTOR, MENTEE, CURATOR, LECTOR
}
