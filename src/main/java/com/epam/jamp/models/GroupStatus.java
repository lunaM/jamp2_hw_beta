package com.epam.jamp.models;

public enum GroupStatus {
	INITIATION, IN_PROGRESS, FINISHED, CANCELED
}
