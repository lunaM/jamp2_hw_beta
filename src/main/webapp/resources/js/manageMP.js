function edit(id) {
	var loc = document.getElementById(id + 'location');
	var start = document.getElementById(id + 'start');
	var end = document.getElementById(id + 'end');

}
window.onload = function() {
	document.getElementById('addNewMP').addEventListener('click', function() {
		var mpName = document.getElementById('mpName').value;
		var mpOfficeLoc = document.getElementById('mpOfficeLoc').value;
		var startDate = document.getElementById('startDate').value;
		var endDate = document.getElementById('endDate').value;
		addMentorshipProgram(mpName, mpOfficeLoc, startDate, endDate);
	});
}
function addMentorshipProgram(mpName, mpOfficeLoc, startDate, endDate) {
	var id = document.getElementById('mpId').value;
	if (!mpName || !mpOfficeLoc || !startDate || !endDate) {
		document.getElementById('alert').style.display = 'inherit';
		return;
	}
	if(!id){
		id = 0;
	}
	$.ajax({
		type : 'POST',
		url : '/addMentorshipProgram',
		data : {
			name : mpName,
			officeLoc : mpOfficeLoc,
			startDate : startDate,
			endDate : endDate,
			id : id
		},
		complete : function(resp) {
			if (resp.responseJSON) {
				console.log(resp.responseJSON);
				document.location.href = '/';
			} else {
				document.getElementById('alert').style.display = 'inherit';
			}

		}
	});
}