<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<c:url value="/resources/css/style.css" />">
<title>LMS</title>
</head>
<body>
	<h1>Learning Management System</h1>
	<input type="hidden" id="mpId" value="${phase.mp.id }">
	<h2>Mentorship program phase </h2><input type="hidden" id="mppId" value="${phase.id }"/>
	<h3>Participants</h3>
	<form:form  method="POST" action="/saveParticipantsChanges" modelAttribute="phase">
		<form:hidden path="id"/>
		<form:hidden path="mp.id"/>
		<table id="participants">
			<thead>
				<tr>
					<td>Email</td>
					<td>Role</td>
					<td>Status</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${phase.participants}" var="person" varStatus="status">
					<tr>
					<td style="display: none;"><form:hidden path="participants[${status.index}].id"/></td>
						<td><form:hidden path="participants[${status.index}].person.email"/>${person.person.email }</td>
						<td><form:select path="participants[${status.index}].role">
								<form:option value="${person.role }" selected="selected">${person.role }</form:option>
								<form:option value="CURATOR">CURATOR</form:option>
				 				<form:option value="LECTOR">LECTOR</form:option>
				 				<form:option value="MENTOR">MENTOR</form:option>
				 				<form:option value="MENTEE">MENTEE</form:option>
							</form:select></td>
						<td><form:select path="participants[${status.index}].status">
								<form:option selected="selected" value="${person.status }">${person.status }</form:option>
					 			<form:option value="PROPOSED">PROPOSED</form:option>
					 			<form:option value="APPROVED_RM">APPROVED_RM</form:option>
					 			<form:option value="CONFIRMED_CDP">CONFIRMED_CDP</form:option>
					 			<form:option value="ON_HOLD">ON_HOLD</form:option>
							</form:select>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<input type="submit" id="save" value="Save changes">
	</form:form>
	<hr>
	<h4>Add participants</h4>
	<div><input type="text" id="email" placeholder="Email"> 
		 Role:  <select id="role">
		 			<option>MENTOR</option>
		 			<option selected="selected">MENTEE</option>
		 			<option>CURATOR</option>
		 			<option>LECTOR</option>
				</select>
		 Status:<select id="status">			 			
		 			<option selected="selected">PROPOSED</option>			 			
		 			<option>APPROVED_RM</option>
		 			<option>CONFIRMED_CDP</option>
		 			<option>ON_HOLD</option>
				</select>
	</div>	
	<button id="addPerson">Add</button>
	<hr>
	<h4>Lectures</h4>
	<table id="lectures">
		<thead>
			<tr>
				<td>Domain area</td>
				<td>Topic</td>
				<td>Lector</td>
				<td>Duration</td>
				<td>Sheduled time</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${phase.lectures}" var="lecture" varStatus="status">
				<tr>
					<td>${lecture.domainArea}</td>
					<td>${lecture.topic}</td>
					<td>${lecture.lector.id}</td>
					<td>${lecture.duration}</td>
					<td>${lecture.sheduledTime}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<hr>
	<h4>Add lecture</h4>
	<form  method="POST" action="/addLecture">
		<input type="hidden" name="phaseId" value="${phase.id }"/>
		<input type="hidden" name="mpId" value="${phase.mp.id }">
		<table>
			<tbody>
				<tr>
					<td><input name="domainArea" placeholder="Domain Area" required="required"/></td>
					<td><input name="topic" placeholder="Topic" required="required"/></td>
					<td><input name="lectorId" placeholder="Lector Email" required="required"/></td>
					<td><input name="duration" placeholder="Duration" required="required"/></td>
					<td><input name="sheduledTime" type="date" required="required"/></td>
				</tr>
			</tbody>
		</table>
		<input type="submit" id="save" value="Add lecture">
	</form>
	
	<hr>
	<h4>Group</h4>
	<form:form  method="POST" action="/saveGroup" modelAttribute="phase">
		<form:hidden path="id"/>
		<form:hidden path="mp.id"/>
		<div>${mp.id}</div>
		<div>
			Planned start <form:input path="group.plannedStart" type="date"/>
		</div>
		<div>
			Planned end <form:input path="group.plannedEnd" type="date"/>
		</div>
		<div>
			Actual start <form:input path="group.actualStart" type="date"/>
		</div>
		<div>
			Actual end <form:input path="group.actualEnd" type="date"/>
		</div>
		Status <form:select path="group.status">
			<option selected="selected">INITIATION</option>
 			<option>IN_PROGRESS</option>
 			<option>FINISHED</option>
 			<option>CANCELED</option>
		</form:select>
		<table id="group">
			<thead>
				<tr>
					<td>Mentee</td>
					<td>Mentor</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${phase.group.pair}" var="pair" varStatus="status">
					<tr>
						<td>${pair.mentee}</td>
						<td>${pair.mentor}</td>
						<td id="${status.index}" onclick="removeMentees(this.id)">x</td>
					</tr>
				</c:forEach>		
			</tbody>
		</table>
		<input type="submit" id="save" value="Save changes">
	</form:form>
	<h4>Add mentee and mentor</h4>
	<div>
		<input type="text" id="mentee" placeholder="Mentee email">
		<input type="text" id="mentor" placeholder="Mentor email">
		<input type="button" value="Add" onclick="addNewPair()">
	</div>
	
	<div class="alert alert-danger" style="display:none" id="alert">
  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  		<strong>Error!</strong> <div id="alertMsg"><h5>Ther's no required person <a href="/addPerson">Add a person to database</a></h5></div>
	</div>
	
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		function addNewPair(){
			var mentee = document.getElementById('mentee').value;
			var mentor = document.getElementById('mentor').value;
			var mppId = document.getElementById('mppId').value;
			$.ajax({
				type: 'POST',
		        url: '/addMenteeToGroup',
		        data: {
		        	mppId : mppId,
		        	mentee : mentee,
		        	mentor : mentor
		        },
		        complete: function (resp) {
		        	console.log(resp);
		        	if(resp.status == 200){
		        		console.log(resp.responseJSON);
		        		location.reload();
		        	}else{
		        		document.getElementById('alert').style.display = 'inherit';
		        		document.getElementById('alertMsg').innerHTML = 'An error occured. Unable to add';
		        	}
		        	
		        }
			});
		}
		function removeMentees(id){
			console.log(id);
			var nodes = document.getElementById(id).parentNode.childNodes;
			var mentee = nodes[1].innerText;
			var mentor = nodes[3].innerText;
			console.log(mentee+' '+mentor);
			var mppId = document.getElementById('mppId').value;
			$.ajax({
				type: 'POST',
		        url: '/removeMenteeFromGroup',
		        data: {
		        	mppId : mppId,
		        	mentee : mentee,
		        	mentor : mentor
		        },
		        complete: function (resp) {
		        	console.log(resp);
		        	if(resp.status == 200){
		        		console.log(resp.responseJSON);
		        		$('#'+id).parent().remove();
		        	}else{
		        		document.getElementById('alertMsg').innerHTML = 'An error occured. Unable to remove';
		        		document.getElementById('alert').style.display = 'inherit';
		        	}
		        	
		        }
			});
		}
		window.onload = function(){
			document.getElementById('addPerson').addEventListener('click',
					function(){
						var email = document.getElementById('email').value;
						var e = document.getElementById('role');
						var role = e.options[e.selectedIndex].value;
						e = document.getElementById('status');
						var status = e.options[e.selectedIndex].value;
						var mpId = document.getElementById('mpId').value;
						var mppId = document.getElementById('mppId').value;
						if(!email || !status || !role){
							alert('All fields are required');
							return;
						}
						$.ajax({
							type: 'POST',
					        url: '/addParticipant',
					        data: {
					        	email : email,
					        	role : role,
					        	status : status,
					        	mpId : mpId,
					        	mppId : mppId
					        },
					        complete: function (resp) {
					        	console.log(resp);
					        	if(resp.status == 200){
					        		console.log(resp.responseJSON);
					        		location.reload();
					        	}else{
					        		document.getElementById('alert').style.display = 'inherit';
					        	}
					        	
					        }
						});
				});
		}

	</script>
</body>
</html>