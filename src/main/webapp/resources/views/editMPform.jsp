<input type="hidden" id="mpId" value="${mp.id }">
<div>
	Name <input type="text" id="mpName" placeholder="Name" value="${mp.name}">
</div>
<div>
	Office location
	<input type="text" id="mpOfficeLoc" placeholder="Office location" value="${mp.officeLocation }">
</div>
<div>
	Start 
	<input type="date" id="startDate" value="${mp.start }">
</div>
<div>
	End
	<input type="date" id="endDate" value="${mp.end }">
</div>
<input type="button" id="addNewMP" value="Add">
<div class="alert alert-danger" style="display: none" id="alert">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	<strong>Error!</strong> Unable to add. Invalid input values.
</div>