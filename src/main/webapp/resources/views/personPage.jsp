<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<c:url value="/resources/css/style.css" />">
<title>LMS</title>
</head>
<body>
	<h1>Learning Management System</h1>
	<h2>Registered persons </h3>
		<c:forEach items="${people}" var="person">
			<table>
				<tr>
					<td>Email</td>
					<td>${person.email }</td>
				</tr>
				<tr>
					<td>Name</td>
					<td>${person.name }</td>
				</tr>
				<tr>
					<td>Surname</td>
					<td>${person.surname }</td>
				</tr>
				<tr>
					<td>Level</td>
					<td>${person.level }</td>
				</tr>
				<tr>
					<td>Primary skill</td>
					<td>${person.primarySkill }</td>
				</tr>
				<tr>
					<td>Manager</td>
					<td>${person.manager }</td>
				</tr>
				<tr>
					<td>Birth date</td>
					<td>${person.birthDate }</td>
				</tr>
				<tr>
					<td>Creation date</td>
					<td>${person.createdDate }</td>
				</tr>
				<tr>
					<td>Created by</td>
					<td>${person.createdBy }</td>
				</tr>
				<tr>
					<td>Modification date</td>
					<td>${person.lastModified }</td>
				</tr>
				<tr>
					<td>Modified by</td>
					<td>${person.modifiedBy }</td>
				</tr>
			</table>
			<hr>
		</c:forEach>
	</table>
	<h3>Add new person or modify existed</h3>
	<div class="form-group input-person">
		<form:form  method="POST" action="/addPerson" modelAttribute="newPerson">
			<form:input type="text" path="name" placeholder="Name" class="form-control" required="required"></form:input>
			<form:input type="text" path="surname" placeholder="Surname" class="form-control" required="required"></form:input>
			<form:input type="text" path="email" placeholder="Email" class="form-control" required="required"></form:input>
			<form:errors path="email" cssclass="error"></form:errors>
			<form:select path="level" required="required">
				<form:option value="L1">L1</form:option>
				<form:option value="L2">L2</form:option>
				<form:option value="L3">L3</form:option>
				<form:option value="L4">L4</form:option>
			</form:select>
			<form:input type="text" path="primarySkill" required="required" placeholder="PrimarySkill" class="form-control"></form:input>
			<form:input type="text" path="manager" placeholder="Manager" required="required" class="form-control"></form:input>
			<form:errors path="birthDate" cssclass="error"></form:errors>
			<form:input type="date" path="birthDate" required="required"></form:input>
			<input type="submit" id="addPerson">
		</form:form>
	</div>
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>