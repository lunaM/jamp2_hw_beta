-- MySQL dump 10.13  Distrib 5.5.46, for Win64 (x86)
--
-- Host: localhost    Database: jamp
-- ------------------------------------------------------
-- Server version	5.5.46

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (5),(5),(5);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lecture`
--

DROP TABLE IF EXISTS `lecture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lecture` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `lector_id` bigint(20) DEFAULT NULL,
  `phase_id` bigint(20) DEFAULT NULL,
  `domain_area` varchar(255) DEFAULT NULL,
  `duration` bigint(20) DEFAULT NULL,
  `sheduled_time` datetime DEFAULT NULL,
  `topic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKel5uu2w5pr0tctqxqg56wrl5a` (`lector_id`),
  KEY `FKpwwaa0bj2ed2jmlks3ki4safa` (`phase_id`),
  CONSTRAINT `FKel5uu2w5pr0tctqxqg56wrl5a` FOREIGN KEY (`lector_id`) REFERENCES `participant` (`id`),
  CONSTRAINT `FKpwwaa0bj2ed2jmlks3ki4safa` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lecture`
--

LOCK TABLES `lecture` WRITE;
/*!40000 ALTER TABLE `lecture` DISABLE KEYS */;
INSERT INTO `lecture` VALUES (2,NULL,2,'Java',120,'2022-12-11 22:00:00','Spring AOP'),(3,NULL,3,'.Net',120,'2018-12-11 22:00:00','.NET Programming Concepts'),(4,NULL,5,'Java',125,'2016-12-11 22:00:00','Spring Data');
/*!40000 ALTER TABLE `lecture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mentorshipprogram`
--

DROP TABLE IF EXISTS `mentorshipprogram`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mentorshipprogram` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `office_location` varchar(255) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKi56xucscao0111p41x6ly6g2x` (`name`,`office_location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mentorshipprogram`
--

LOCK TABLES `mentorshipprogram` WRITE;
/*!40000 ALTER TABLE `mentorshipprogram` DISABLE KEYS */;
INSERT INTO `mentorshipprogram` VALUES (2,'28fefe26-868a-47d6-916a-d3af6b942f1b','2016-10-16 09:09:36','2019-12-11 22:00:00','2016-10-16 09:13:52','ec4f511c-55e2-42e4-b082-a3d7d5bddc6d','JaMP2','Lviv','2016-12-11 22:00:00'),(3,NULL,NULL,'2019-12-11 22:00:00',NULL,NULL,'.Net','Lviv','2018-12-11 22:00:00'),(4,NULL,NULL,'2017-12-11 22:00:00',NULL,NULL,'JaMP','Kyiv','2016-12-11 22:00:00');
/*!40000 ALTER TABLE `mentorshipprogram` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pair`
--

DROP TABLE IF EXISTS `pair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pair` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) DEFAULT NULL,
  `mentee` varchar(255) DEFAULT NULL,
  `mentor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpdq001w2478pg0pesx39g4wwt` (`group_id`),
  CONSTRAINT `FKpdq001w2478pg0pesx39g4wwt` FOREIGN KEY (`group_id`) REFERENCES `phase_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pair`
--

LOCK TABLES `pair` WRITE;
/*!40000 ALTER TABLE `pair` DISABLE KEYS */;
INSERT INTO `pair` VALUES (8,2,'antony@mm.com','antony@mm.com'),(9,2,'antony@mm.com','vik100@meta.ua'),(11,3,'antony@mm.com','antony@mm.com'),(12,3,'vik100@meta.ua','antony@mm.com'),(13,5,'vik100@meta.ua','vik100@meta.ua'),(14,5,'berry@meta.com','vik100@meta.ua');
/*!40000 ALTER TABLE `pair` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participant`
--

DROP TABLE IF EXISTS `participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `phase_id` bigint(20) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6l61hfn69hie0s97ihwoov0wy` (`email`),
  KEY `FKnickbg1yd3ki8n94l57dxgvhb` (`phase_id`),
  CONSTRAINT `FK6l61hfn69hie0s97ihwoov0wy` FOREIGN KEY (`email`) REFERENCES `people` (`email`),
  CONSTRAINT `FKnickbg1yd3ki8n94l57dxgvhb` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participant`
--

LOCK TABLES `participant` WRITE;
/*!40000 ALTER TABLE `participant` DISABLE KEYS */;
INSERT INTO `participant` VALUES (3,'vik100@meta.ua',2,1,1),(4,'vik100@meta.ua',2,2,0),(5,'vik100@meta.ua',2,3,1),(6,'vik100@meta.ua',2,0,0),(7,'antony@mm.com',2,1,0),(8,'antony@mm.com',2,0,0),(10,'vik100@meta.ua',3,1,0),(11,'vik100@meta.ua',3,0,0),(12,'antony@mm.com',3,1,0),(13,'antony@mm.com',3,0,0),(14,'vik100@meta.ua',4,1,0),(15,'antony@mm.com',4,1,0),(16,'vik100@meta.ua',5,1,0),(17,'vik100@meta.ua',5,0,0),(18,'berry@meta.com',5,1,0),(19,'berry@meta.com',5,3,0);
/*!40000 ALTER TABLE `participant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `people`
--

DROP TABLE IF EXISTS `people`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people` (
  `email` varchar(255) NOT NULL,
  `birth_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `manager` varchar(255) DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `primarySkill` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `people`
--

LOCK TABLES `people` WRITE;
/*!40000 ALTER TABLE `people` DISABLE KEYS */;
INSERT INTO `people` VALUES ('antony@mm.com','1990-03-02 22:00:00','d5a9ebb9-c705-496e-bab6-413b7cf2d021','2016-10-16 14:33:23',NULL,0,'Bob Marley',NULL,'Antony','Java','Hoppking'),('berry@meta.com','1996-12-11 22:00:00',NULL,NULL,NULL,0,'Bob Marley',NULL,'Melon','Java','Berry'),('vik100@meta.ua','1996-12-11 22:00:00','c8de910d-f1f9-49a4-af4e-f5a50ccf54c1','2016-10-16 09:10:20',NULL,0,'Bob Marley',NULL,'Viktoriya','Java','Skywalker');
/*!40000 ALTER TABLE `people` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phase`
--

DROP TABLE IF EXISTS `phase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phase` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mp_id` bigint(20) DEFAULT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKal040hawpd5eeawh2x2gb2l4t` (`mp_id`),
  KEY `FKl8dd1opamjui6vp422yd7b74y` (`group_id`),
  CONSTRAINT `FKal040hawpd5eeawh2x2gb2l4t` FOREIGN KEY (`mp_id`) REFERENCES `mentorshipprogram` (`id`),
  CONSTRAINT `FKl8dd1opamjui6vp422yd7b74y` FOREIGN KEY (`group_id`) REFERENCES `phase_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phase`
--

LOCK TABLES `phase` WRITE;
/*!40000 ALTER TABLE `phase` DISABLE KEYS */;
INSERT INTO `phase` VALUES (2,2,2,NULL,NULL,'2016-10-16 18:45:24','072f2727-878c-4028-8f84-8f6c7bdcbd41','Phase1'),(3,3,3,'40ce10e0-07b9-4c3f-804b-df85d6e1475f','2016-10-16 19:59:55','2016-10-16 20:02:41','7fd291d7-78a3-47c9-9ecc-217bf622de28','Phase1'),(4,4,NULL,'ded7c35e-596b-4af0-80af-c4171bdb44c8','2016-10-16 21:51:53','2016-10-16 22:01:04','3ab0c868-2ecd-4b59-8bad-993cff11321a','Phase1'),(5,2,5,'e7e0fa0e-a468-4666-86ea-e9b92d5e94ea','2016-10-17 08:43:03','2016-10-17 09:41:46','95b97b00-9518-48af-ba01-84739fb81d13','Phase2');
/*!40000 ALTER TABLE `phase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phase_group`
--

DROP TABLE IF EXISTS `phase_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phase_group` (
  `id` bigint(20) NOT NULL,
  `actual_end` datetime DEFAULT NULL,
  `actual_start` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `planned_end` datetime DEFAULT NULL,
  `planned_start` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phase_group`
--

LOCK TABLES `phase_group` WRITE;
/*!40000 ALTER TABLE `phase_group` DISABLE KEYS */;
INSERT INTO `phase_group` VALUES (2,'2016-10-25 21:00:00','2016-10-04 21:00:00',NULL,NULL,'2016-10-16 15:29:23','59100713-de70-4298-b475-c44cc0858cab','2016-10-25 21:00:00','2016-10-04 21:00:00',0),(3,'2019-12-11 22:00:00','2015-12-11 22:00:00',NULL,NULL,'2016-10-16 20:02:07','18087a43-f0ef-46cb-9720-4e8aecf302c5','2019-12-11 22:00:00','2015-12-11 22:00:00',0),(5,'2023-12-11 22:00:00','2016-12-11 22:00:00',NULL,NULL,'2016-10-17 08:44:17','62aed8f9-0565-4b32-b294-c31aab568990','2023-12-11 22:00:00','2016-12-11 22:00:00',0);
/*!40000 ALTER TABLE `phase_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-17 13:01:18
